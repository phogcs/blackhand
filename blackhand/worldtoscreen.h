#ifndef WORLDTOSCREEN_H
#define WORLDTOSCREEN_H

#include "sdk.h"

bool world_to_screen(const struct vector *world, struct vector *screen);

#endif