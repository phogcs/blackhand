#include "ivpanel.h"
#include "interfaces.h"

const char *get_panel_name(VPanel vgui)
{
	const get_panel_name_function get_panel_name_fn = (*(void ***) panel)[36];
	return get_panel_name_fn(panel, 0, vgui);
}