#ifndef COLOR_H
#define COLOR_H

struct color {
	int r, g, b, a;
};

struct color create_color(int r, int g, int b, int a);

#endif