#ifndef VISUALS_H
#define VISUALS_H

#include "colors.h"
#include "fonts.h"
#include "esp.h"
#include "watermark.h"

void start_visuals(void);

#endif