#ifndef VECTOR_H
#define VECTOR_H

struct matrix4x4 {
	float m[4][4];
};

struct vector {
	float x, y, z;
};

struct vector create_vector(float x, float y, float z);
struct vector add_vector(struct vector *a, struct vector *b);

#endif