#ifndef ENTITY_H
#define ENTITY_H

#include <stdbool.h>

#include "definitions.h"

#define OFFSET_TEAM 0xF0
#define OFFSET_HEALTH 0xFC
#define OFFSET_ORIGIN 0x134
#define OFFSET_COLLISION 0x320
#define OFFSET_DORMANT 0xE9

enum TeamID get_entity_team(HEntity entity);
int get_entity_health(HEntity entity);
struct vector get_entity_origin(HEntity entity);
struct collision_property *get_entity_collision(HEntity entity);
bool get_entity_dormant(HEntity entity);

#endif