#ifndef ENTITYCLIENT_H
#define ENTITYCLIENT_H

#include <stdbool.h>

#include "definitions.h"
#include "vector.h"

typedef int (__fastcall *get_local_player_index_function)(void *engineclient, void *edx);
int get_local_player_index(void);
HEntity get_local_player(void);

typedef bool (__fastcall *is_in_game_function)(void *engineclient, void *edx);
bool is_in_game(void);

typedef bool (__fastcall *is_connected_function)(void *engineclient, void *edx);
bool is_connected(void);

typedef struct matrix4x4 *(__fastcall *get_world_to_screen_matrix_function)(void *engineclient, void *edx);
struct matrix4x4 *get_world_to_screen_matrix(void);

#endif