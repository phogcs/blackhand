#include "colors.h"

struct color watermark_color;
struct color esp_friendly_color;
struct color esp_enemy_color;

void start_colors(void)
{
	watermark_color = create_color(255, 255, 255, 255);
	esp_friendly_color = create_color(66, 134, 244, 255);
	esp_enemy_color = create_color(244, 66, 66, 255);
}