#include <stdlib.h>

#include "hooks.h"
#include "interfaces.h"
#include "vt.h"

void **panel_vmt;
void **panel_vmt_original;

void start_hooks(void)
{
	panel_vmt = hook_vt(panel, &panel_vmt_original);
	panel_vmt[41] = painttraverse;
}

void close_hooks(void)
{
	unhook_vt(panel, panel_vmt_original);
	free(panel_vmt);
	free(panel_vmt_original);
}