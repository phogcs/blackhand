#include "entitylist.h"
#include "interfaces.h"

HEntity get_entity(int index)
{
	const get_entity_function get_entity_fn = (*(void ***) entitylist)[3];
	return get_entity_fn(entitylist, 0, index);
}

int get_highest_entity_index(void)
{
	const get_highest_entity_index_function get_highest_entity_index_fn = (*(void ***) entitylist)[6];
	return get_highest_entity_index_fn(entitylist, 0);
}