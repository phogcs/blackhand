#include "fonts.h"

HFont watermark_font;

void start_fonts(void)
{
	set_font_glyph_set(watermark_font = create_font(), "Terminal", 12, 400, 0, 0, FONTFLAG_OUTLINE, 0, 0);
}