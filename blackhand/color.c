#include "color.h"

struct color create_color(int r, int g, int b, int a)
{
	struct color col;
	col.r = r;
	col.g = g;
	col.b = b;
	col.a = a;
	return col;
}