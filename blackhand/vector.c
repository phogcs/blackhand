#include "vector.h"

struct vector create_vector(float x, float y, float z)
{
	struct vector vec;
	vec.x = x;
	vec.y = y;
	vec.z = z;
	return vec;
}

struct vector add_vector(struct vector *a, struct vector *b)
{
	struct vector vec;
	vec.x = a->x + b->x;
	vec.y = a->y + b->y;
	vec.z = a->z + b->z;
	return vec;
}