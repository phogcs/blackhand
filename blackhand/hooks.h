#ifndef HOOKS_H
#define HOOKS_H

#include <stdbool.h>

#include "sdk.h"

typedef void (__fastcall *painttraverse_function)(void *panel, void *edx, unsigned int vgui, bool force_repaint, bool allow_force);
void __stdcall painttraverse(VPanel vgui, bool force_repaint, bool allow_force);

void start_hooks(void);
void close_hooks(void);

extern void **panel_vmt_original;

#endif