#include <stddef.h>

#include "vt.h"

size_t get_vt_size(void *vt)
{
	size_t size = 0;
	while ((void *)(*(uintptr_t**) vt)[size])
		size++;

	return size;
}

void **hook_vt(void *vt, void ***original)
{
	void **old = *(void ***) vt;
	const size_t size = get_vt_size(vt);
	void **modified = (void **) malloc(size * sizeof(void*));

	*original = old;
	memcpy(modified, old, size * sizeof(void*));

	*(void ***) vt = modified;
	return modified;
}

void unhook_vt(void *vt, void **original)
{
	*(void ***) vt = original;
}