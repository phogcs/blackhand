#ifndef VT_H
#define VT_H

void **hook_vt(void *vt, void ***original);
void unhook_vt(void *vt, void **original);

#endif