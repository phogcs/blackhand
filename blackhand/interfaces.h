#ifndef INTERFACES_H
#define INTERFACES_H

typedef void *(*interface_function)(const char *, int);

void start_interfaces(void);
void close_interfaces(void);

extern void *engineclient;
extern void *entitylist;
extern void *panel;
extern void *surface;

#endif