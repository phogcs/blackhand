#include "watermark.h"
#include "colors.h"
#include "fonts.h"

const wchar_t* watermark = L"blackhand by phog";

void draw_watermark(void)
{
	draw_set_text_color(watermark_color.r, watermark_color.g, watermark_color.b, watermark_color.a);
	draw_set_text_font(watermark_font);
	draw_set_text_pos(0, 0);
	draw_print_text(watermark, wcslen(watermark), FONT_DRAW_DEFAULT);
}