#include "esp.h"
#include "worldtoscreen.h"

void get_esp_points(HEntity entity, float *min_x, float *max_x, float *min_y, float *max_y)
{
	const struct collision_property *collision = get_entity_collision(entity);
	const struct vector origin = get_entity_origin(entity);
	struct vector mins = add_vector(&origin, &collision->mins);
	struct vector maxs = add_vector(&origin, &collision->maxs);

	const struct vector origins[] = {
		create_vector(mins.x, mins.y, mins.z),
		create_vector(mins.x, maxs.y, mins.z),
		create_vector(maxs.x, maxs.y, mins.z),
		create_vector(maxs.x, mins.y, mins.z),
		create_vector(maxs.x, maxs.y, maxs.z),
		create_vector(mins.x, maxs.y, maxs.z),
		create_vector(mins.x, mins.y, maxs.z),
		create_vector(maxs.x, mins.y, maxs.z)
	};

	struct vector points[8];

	for (int i = 0; i < 8; ++i)
		if (!world_to_screen(&origins[i], &points[i]))
			return;

	float left = points[0].x;
	float right = points[0].x;
	float top = points[0].y;
	float bottom = points[0].y;

	for (int i = 1; i < 8; ++i) {
		if (left > points[i].x)
			left = points[i].x;
		
		if (right < points[i].x)
			right = points[i].x;

		if (top > points[i].y)
			top = points[i].y;

		if (bottom < points[i].y)
			bottom = points[i].y;
	}

	*min_x = left;
	*max_x = right;
	*min_y = top;
	*max_y = bottom;
}

void draw_esp(HEntity entity, struct color *col)
{
	float min_x, max_x, min_y, max_y;
	get_esp_points(entity, &min_x, &max_x, &min_y, &max_y);

	draw_set_color(0, 0, 0, 255);
	draw_outlined_rect(min_x - 1, min_y - 1, max_x + 1, max_y + 1);
	draw_outlined_rect(min_x + 1, min_y + 1, max_x - 1, max_y - 1);

	draw_set_color(col->r, col->g, col->b, col->a);
	draw_outlined_rect(min_x, min_y, max_x, max_y);
}