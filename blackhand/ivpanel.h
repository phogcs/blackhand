#ifndef IVPANEL_H
#define IVPANEL_H

#include "definitions.h"

typedef const char *(__fastcall *get_panel_name_function)(void *panel, void *edx, VPanel vgui);
const char *get_panel_name(VPanel vgui);

#endif