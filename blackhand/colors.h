#ifndef COLORS_H
#define COLORS_H

#include "sdk.h"

void start_colors(void);

extern struct color watermark_color;
extern struct color esp_friendly_color;
extern struct color esp_enemy_color;

#endif