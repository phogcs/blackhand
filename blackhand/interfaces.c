#include <Windows.h>

#include "interfaces.h"

void *engineclient;
void *entitylist;
void *panel;
void *surface;

void *grab_interface(const char *module, const char *name)
{
	interface_function interface_fn = (interface_function) GetProcAddress(GetModuleHandle(module), "CreateInterface");
	return (void *) interface_fn(name, 0);
}

void start_interfaces(void)
{
	engineclient = grab_interface("engine.dll", "VEngineClient014");
	entitylist = grab_interface("client.dll", "VClientEntityList003");
	panel = grab_interface("vgui2.dll", "VGUI_Panel009");
	surface = grab_interface("vguimatsurface.dll", "VGUI_Surface031");
}

void close_interfaces(void)
{
	free(engineclient);
	free(entitylist);
	free(panel);
	free(surface);
}