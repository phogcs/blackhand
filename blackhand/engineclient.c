#include "engineclient.h"
#include "interfaces.h"
#include "entitylist.h"


int get_local_player_index(void)
{
	const get_local_player_index_function get_local_player_index_fn = (*(void ***) engineclient)[12];
	return get_local_player_index_fn(engineclient, 0);
}

HEntity get_local_player(void)
{
	return get_entity(get_local_player_index());
}

bool is_in_game(void)
{
	const is_in_game_function is_in_game_fn = (*(void ***) engineclient)[26];
	return is_in_game_fn(engineclient, 0);
}

bool is_connected(void)
{
	const is_connected_function is_connected_fn = (*(void ***) engineclient)[27];
	return is_connected_fn(engineclient, 0);
}

struct matrix4x4 *get_world_to_screen_matrix(void)
{
	const get_world_to_screen_matrix_function get_world_to_screen_matrix_fn = (*(void ***) engineclient)[37];
	return get_world_to_screen_matrix_fn(engineclient, 0);
}