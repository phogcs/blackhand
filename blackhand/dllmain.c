#include <Windows.h>
#include <stdbool.h>
#include <stdio.h>

#include "interfaces.h"
#include "hooks.h"
#include "hacks.h"

HANDLE thread;

void start()
{
	AllocConsole();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);

	start_interfaces();
	start_hooks();
	start_visuals();
}

void close()
{
	close_hooks();
	close_interfaces();
}

bool WINAPI DllMain(HINSTANCE instance, DWORD reason, LPVOID reserved)
{
	switch (reason) {
	case DLL_PROCESS_ATTACH:
		thread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE) start, 0, 0, 0);
		break;

	case DLL_PROCESS_DETACH:
		FreeLibraryAndExitThread((HMODULE) thread, 0);
		close();
		break;
	}

	return true;
}
