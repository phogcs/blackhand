#ifndef ENTITYLIST_H
#define ENTITYLIST_H

#include "definitions.h"

typedef HEntity (__fastcall *get_entity_function)(void *entitylist, void *edx, int index);
HEntity get_entity(int index);

typedef int (__fastcall *get_highest_entity_index_function)(void *entitylsit, void *edx);
int get_highest_entity_index(void);

#endif