#include "entity.h"

enum TeamID get_entity_team(HEntity entity)
{
	return *(enum TeamID *)(entity + OFFSET_TEAM);
}

int get_entity_health(HEntity entity)
{
	return *(int *)(entity + OFFSET_HEALTH);
}

struct vector get_entity_origin(HEntity entity)
{
	return *(struct vector *)(entity + OFFSET_ORIGIN);
}

struct collision_property *get_entity_collision(HEntity entity)
{
	return (struct collision_property *)(entity + OFFSET_COLLISION);
}

bool get_entity_dormant(HEntity entity)
{
	return *(bool *)(entity + OFFSET_DORMANT);
}