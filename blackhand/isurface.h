#ifndef ISURFACE_H
#define ISURFACE_H

#include <wchar.h>
#include <stdbool.h>

#include "color.h"
#include "definitions.h"

typedef void (__fastcall *draw_set_color_function)(void *surface, void *edx, int r, int g, int b, int a);
void draw_set_color(int r, int g, int b, int a);

typedef void (__fastcall *draw_filled_rect_function)(void *surface, void *edx, int x, int y, int x2, int y2);
void draw_filled_rect(int x, int y, int x2, int y2);

typedef void (__fastcall *draw_outlined_rect_function)(void *surface, void *edx, int x, int y, int x2, int y2);
void draw_outlined_rect(int x, int y, int x2, int y2);

typedef void (__fastcall *draw_outlined_rect_function)(void *surface, void *edx, int x, int y, int x2, int y2);
void draw_outlined_rect(int x, int y, int x2, int y2);

typedef void (__fastcall *draw_line_function)(void *surface, void *edx, int x, int y, int x2, int y2);
void draw_line(int x, int y, int x2, int y2);

typedef void (__fastcall *draw_set_text_font_function)(void *surface, void *edx, HFont font);
void draw_set_text_font(HFont font);

typedef void (__fastcall *draw_set_text_color_function)(void *surface, void *edx, int r, int g, int b, int a);
void draw_set_text_color(int r, int g, int b, int a);

typedef void (__fastcall *draw_set_text_pos_function)(void *surface, void *edx, int x, int y);
void draw_set_text_pos(int x, int y);

typedef void (__fastcall *draw_print_text_function)(void *surface, void *edx, const wchar_t *text, int len, enum FontDrawType_t type);
void draw_print_text(const wchar_t *text, int len, enum FontDrawType_t type);

typedef void (__fastcall *get_screen_size_function)(void *surface, void *edx, int *width, int *height);
void get_screen_size(int *width, int *height);

typedef HFont (__fastcall *create_font_function)(void *surface, void *edx);
HFont create_font(void);

typedef bool (__fastcall *set_font_glyph_set_function)(void *surface, void *edx, HFont font, const char* name, int tall, int weight, int blur, int scanlines, int flags, int range_min, int range_max);
bool set_font_glyph_set(HFont font, const char* name, int tall, int weight, int blur, int scanlines, int flags, int range_min, int range_max);

typedef void (__fastcall *get_text_size_function)(void *surface, void *edx, HFont font, const wchar_t* text, int* wide, int* tall);
void get_text_size(HFont font, const wchar_t* text, int* wide, int* tall);

#endif