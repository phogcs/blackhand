#include "worldtoscreen.h"

bool world_to_screen(struct vector *world, struct vector *screen)
{
	struct matrix4x4 *matrix = get_world_to_screen_matrix();
	float w =
		matrix->m[3][0] * world->x +
		matrix->m[3][1] * world->y +
		matrix->m[3][2] * world->z +
		matrix->m[3][3];

	if (w < 0.001f)
		return false;

	float invw = 1.0f / w;

	int screen_size[2];
	get_screen_size(&screen_size[0], &screen_size[1]);

	screen->x = screen_size[0] / 2.0f + ((matrix->m[0][0] * world->x + matrix->m[0][1] * world->y + matrix->m[0][2] * world->z + matrix->m[0][3]) * invw / 2.0f * screen_size[0] + 0.5f);
	screen->y = screen_size[1] / 2.0f - ((matrix->m[1][0] * world->x + matrix->m[1][1] * world->y + matrix->m[1][2] * world->z + matrix->m[1][3]) * invw / 2.0f * screen_size[1] + 0.5f);
	
	return true;
}