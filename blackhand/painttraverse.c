#include <stdio.h>

#include "interfaces.h"
#include "hooks.h"
#include "hacks.h"
#include "worldtoscreen.h"

struct vector origin;

void __stdcall painttraverse(VPanel vgui, bool force_repaint, bool allow_force)
{
	const painttraverse_function painttraverse_fn = panel_vmt_original[41];
	painttraverse_fn(panel, 0, vgui, force_repaint, allow_force);

	if (!strcmp(get_panel_name(vgui), "FocusOverlayPanel")) {
		draw_watermark();

		if (is_in_game() && is_connected()) {
			const HEntity local_player = get_local_player();
			if (local_player) {
				const enum TeamID local_team = get_entity_team(local_player);

				for (int i = 0; i < get_highest_entity_index(); i++) {
					const HEntity entity = get_entity(i);
					if (!entity)
						continue;

					if (entity == local_player)
						continue;

					const bool dormant = get_entity_dormant(entity);
					if (dormant)
						continue;

					const int health = get_entity_health(entity);
					if (health <= 0)
						continue;

					const enum TeamID team = get_entity_team(entity);
					if (team == local_team)
						draw_esp(entity, &esp_friendly_color);
					else
						draw_esp(entity, &esp_enemy_color);
				}
			}
		}
	}
} 