#include "isurface.h"
#include "interfaces.h"

void draw_set_color(int r, int g, int b, int a)
{
	const draw_set_color_function draw_set_color_fn = (*(void ***) surface)[15];
	draw_set_color_fn(surface, 0, r, g, b, a);
}

void draw_filled_rect(int x, int y, int x2, int y2)
{
	const draw_filled_rect_function draw_filled_rect_fn = (*(void ***) surface)[16];
	draw_filled_rect_fn(surface, 0, x, y, x2, y2);
}

void draw_outlined_rect(int x, int y, int x2, int y2)
{
	const draw_outlined_rect_function draw_outlined_rect_fn = (*(void ***) surface)[18];
	draw_outlined_rect_fn(surface, 0, x, y, x2, y2);
}

void draw_line(int x, int y, int x2, int y2)
{
	const draw_line_function draw_line_fn = (*(void ***) surface)[19];
	draw_line_fn(surface, 0, x, y, x2, y2);
}

void draw_set_text_font(HFont font)
{
	const draw_set_text_font_function draw_set_text_font_fn = (*(void ***) surface)[23];
	draw_set_text_font_fn(surface, 0, font);
}

void draw_set_text_color(int r, int g, int b, int a)
{
	const draw_set_text_color_function draw_set_text_color_fn = (*(void ***) surface)[25];
	draw_set_text_color_fn(surface, 0, r, g, b, a);
}

void draw_set_text_pos(int x, int y)
{
	const draw_set_text_pos_function draw_set_text_pos_fn = (*(void ***) surface)[26];
	draw_set_text_pos_fn(surface, 0, x, y);
}

void draw_print_text(const wchar_t *text, int len, enum FontDrawType_t type)
{
	const draw_print_text_function draw_print_text_fn = (*(void ***) surface)[28];
	draw_print_text_fn(surface, 0, text, len, type);
}

void get_screen_size(int *width, int *height)
{
	const get_screen_size_function get_screen_size_fn = (*(void ***) surface)[44];
	get_screen_size_fn(surface, 0, width, height);
}

HFont create_font(void)
{
	const create_font_function create_font_fn = (*(void ***) surface)[71];
	return create_font_fn(surface, 0);
}

bool set_font_glyph_set(HFont font, const char* name, int tall, int weight, int blur, int scanlines, int flags, int range_min, int range_max)
{
	const set_font_glyph_set_function set_font_glyph_set_fn = (*(void ***) surface)[72];
	return set_font_glyph_set_fn(surface, 0, font, name, tall, weight, blur, scanlines, flags, range_min, range_max);
}

void get_text_size(HFont font, const wchar_t* text, int* wide, int* tall)
{
	const get_text_size_function get_text_size_fn = (*(void ***) surface)[79];
	get_text_size_fn(surface, 0, font, text, wide, tall);
}